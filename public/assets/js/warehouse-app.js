function WarehouseApp (configOverride) {

    /** Save self-context, just in case **/
    var self = this;

    var config = {
        mainContainerSelector   : "#mainContainer",
        articlesContainerSelector : "#articlesContainer",
        productsContainerSelector : "#productsContainer",
        createArticleButtonSelector : ".js-create-article",
        createProductButtonSelector : ".js-create-product",
        createArticleDialogSelector : ".js-create-article-dialog",
        createProductDialogSelector : ".js-create-product-dialog",
        productArticlesSelectSelector : ".js-product-articles-select",
        productArticleTemplateSelector : ".js-product-article-template",
        productArticlePlaceholderSelector : ".js-product-article-placeholder",
        removeProductArticleButtonSelector : ".js-remove-product-article",
        createDialogFormSelector : ".js-create-form",
        closeDialogSelector : ".js-close-dialog",
        sellProductButtonSelector : ".js-sell-product",
        addStockToArticleButtonSelector : ".js-add-stock-to-article",
        /** **/
        resetSourceButtonSelector : ".js-reset-source-files",
        /** Notifications and Loading Overlay CFG **/
        notificationsSelector : "#notification",
        notificationStickTimeMs : 3000, // Time that the notification is visible
        loadingOverlaySelector : "#loadingOverlay",
        /** Config to be populated on Instantiation **/
        rootUrl : ""
    };

    /** Flow control vars **/
    var _init = false,
        _ready = false;

    /** DOM jQuery Objects (start with $) **/
    var $main,
        $resetButton,
        $articlesContainer,
        $productsContainer,
        $notification,
        $loadingOverlay;


    /*****************
     * INITIALISATION *
     ******************/

    /**
     * Merges the config if params have been modified on initialization
     */
    var initializeConfig = function() {
        // Override config if valid and present
        if (configOverride !== undefined && typeof configOverride === "object") {
            config  = $.extend({}, config, configOverride);
        }
    };

    /**
     * Triggered on document ready, performs initial DOM parsing operations
     * and binds events
     * @returns {boolean}
     */
    var ready = function() {
        if (_ready) { return false; }

        // Initialize DOM Objects as jQuery elements
        $main = $(config.mainContainerSelector);
        if ($main.length > 0) {
            // Lets search from main, avoid complete DOM tree search. Slight micro-optimisation tip, lol.
            $articlesContainer = $main.find(config.articlesContainerSelector);
            $productsContainer = $main.find(config.productsContainerSelector);
        }

        $notification = $(config.notificationsSelector);
        $loadingOverlay = $(config.loadingOverlaySelector);
        $resetButton = $(config.resetSourceButtonSelector);

        bindBehaviours();

        self.hideLoadingOverlay();

        _ready = true;
    };

    /**
     * Trigger method, registers a ready listener
     * @returns {boolean}
     */
    var init = function() {
        if (_init) { return false; }

        initializeConfig();

        // Register Ready function
        $(function() {
            ready();
        });
        _init = true;

    };

    /*******************************
     * GENERAL BEHAVIOUR - ACTIONS *
     *******************************/

    /**
     * ASYNC:  Adds stock to an article, refreshes articles the list once completed
     * @param art_id
     */
    var addStockToArticle = function(art_id) {
        if (art_id === undefined) { self.fail("art_id not provided on addStockToArticle"); }
        var data = {
            target: "article",
            action: "add",
            art_id: art_id
        };
        performAsyncCall(data, $articlesContainer, refreshProductsList);
    };

    /**
     * ASYNC: Sells a product, refreshes the products list once completed
     * @param name
     */
    var sellProduct = function(name) {
        if (name === undefined) { self.fail("art_id not provided on sellProduct"); }
        var data = {
            target: "product",
            action: "sell",
            name : name
        };
        performAsyncCall(data, $productsContainer, refreshArticlesList);
    };

    /**
     * ASYNC: Refresh the articles list
     */
    var refreshArticlesList = function() {
        var data = {
            target: "article",
            action: "get"
        };
        performAsyncCall(data, $articlesContainer);
    };

    /**
     * ASYNC: Refresh the product list
     */
    var refreshProductsList = function() {
        var data = {
            target: "product",
            action: "get"
        };
        performAsyncCall(data, $productsContainer);
    };

    /**
     * ASYNC: Reset the source files to it's original state
     */
    var resetSourceFiles = function(onComplete) {
        var data = {
            target: "article",
            action: "reset"
        };
        var onSuccessCallback = function() {
            refreshArticlesList();
            refreshProductsList();
            if (onComplete !== undefined && typeof onComplete === "function") {
                onComplete();
            }
        };
        performAsyncCall(data, undefined, onSuccessCallback);
    };

    /**************
     * UX HELPERS *
     **************/

    /**
     * Notification helper
     * Public visibility in
     */
    this.notify = function(type, message) {
        $notification.removeClass().addClass('text-center alert alert-' + type).html("<h2>" + message + "</h2>").slideDown().delay(config.notificationStickTimeMs).slideUp();
    };

    /**
     * Shows site loading overlay
     */
    this.showLoadingOverlay = function() {
        if (!$loadingOverlay.is(':visible')) {
            $loadingOverlay.stop(true,true).show();
        }
    };

    /**
     * Hides site loading overlay
     */
    this.hideLoadingOverlay = function() {
        if ($loadingOverlay.is(':visible')) {
            $loadingOverlay.stop(true,true).fadeOut(200);
        }
    };

    /**
     * Toggles site loading overlay, just in case we need it later
     */
    this.toggleLoadingOverlay = function() {
        if ($loadingOverlay.is(':visible')) {
            self.hideLoadingOverlay();
        }else{
            self.showLoadingOverlay();
        }
    };

    /**
     * Debugging helper
     * @param msg
     */
    this.fail = function(msg) {
        self.notify("danger", msg);
        console.error("Warehouse Error > " + msg );
    };

    /**
     * Opens a "dialog"
     * @param $dialog
     * @param onComplete
     */
    var openDialog = function($dialog, onComplete) {
        if ($dialog.length > 0) {
            $dialog.slideDown();
        }
        if (onComplete !== undefined && typeof onComplete === "function") {
            onComplete();
        }
    };

    /**
     * Close/Opens provided dialogs
     * @param $elementToClose
     * @param $elementToShow
     */
    this.closeDialog = function($elementToClose, $elementToShow) {
        if (!$elementToClose instanceof $) { $elementToClose = $($elementToClose); }
        if (!$elementToShow instanceof $) { $elementToShow = $($elementToShow); }

        if ($elementToClose.is(':visible')) {
            $elementToClose.slideUp(function() {
                if (!$elementToShow.is(':visible')) {
                    $elementToShow.fadeIn();
                }
            });
        }

    };

    /*********************
     * BEHAVIOUR HELPERS *
     *********************/

    /**
     * Loads a ProductArticle creation form from a template and injects it into the Product Creation form
     * @param articleName
     * @param artId
     * @param onComplete
     */
    var loadProductArticleIntoDialog = function(articleName, artId, onComplete) {
        if (articleName === undefined || artId === undefined) { self.fail("art_id or articleName not provided on loadProductArticleIntoDialog"); }

        var templateHtmlString = $productsContainer.find(config.productArticleTemplateSelector).html();

        // Replace Art_id in the html
        templateHtmlString = templateHtmlString.split("%artId%").join(artId);

        // Replace ArticleName in the html
        templateHtmlString = templateHtmlString.split("%productArticleName%").join(articleName);

        var $template = $(templateHtmlString),
            $templatePlaceholder = $productsContainer.find(config.productArticlePlaceholderSelector);

        if ($templatePlaceholder.length > 0) {
            $templatePlaceholder.before($template);
        }

        if (onComplete !== undefined && typeof onComplete === "function") {
            onComplete();
        }

    };

    /******************************
     * ASYNC CALLS MANAGER METHODS *
     ******************************/

    /**
     * Master method to perform an async operation.
     * Communicates with the backend and parses the responses. It notifies the user or refreshes the dom if needed
     * @param requestData
     * @param $returningHtmlContainer
     * @param onSuccess
     * @param onError
     * @param onComplete
     */
    var performAsyncCall = function(requestData, $returningHtmlContainer, onSuccess, onError, onComplete) {

        //requestData = new URLSearchParams(requestData);

        if (requestData["action"] === "undefined" || requestData["target"] === "undefined") {
            self.fail("Action or Target not present!");
        }

        $.ajax({
            url : config.rootUrl + "app/xhr/"+requestData["target"]+"-actions.php",
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify(requestData),
            beforeSend: function() {
                // self.showLoadingOverlay();
            },
            success : function(response) {
                if (response.hasOwnProperty("success") && response.success === true) {
                    if (response.hasOwnProperty("html")) {
                        var $targetHtmlContainer;
                        if ($returningHtmlContainer !== undefined) {
                            $targetHtmlContainer = $($returningHtmlContainer);
                            if ($targetHtmlContainer.length > 0 ) {
                                $targetHtmlContainer.html(response.html);
                            }
                        }
                    }
                    if (response.hasOwnProperty("notification")) {
                        self.notify("success", response.notification);
                    }
                    // Trigger Callback
                    if (onSuccess !== undefined && typeof onSuccess === "function") {
                        onSuccess();
                    }
                } else {
                    var errorMessage = response.hasOwnProperty("error") ? response.error : "Oops! Something went wrong";
                    self.notify("danger", errorMessage);
                }
            },
            error: function (response) {

                debugger;

                var errorMessage = response.hasOwnProperty("error") ? response.error : "Oops! Something went wrong";
                self.notify("danger", errorMessage);

                // Trigger Callback
                if (onError !== undefined && typeof onError === "function") {
                    onError();
                }
            },
            complete: function(response) {
                //self.hideLoadingOverlay();
                // Trigger Callback
                if (onComplete !== undefined && typeof onComplete === "function") {
                    onComplete();
                }
            }
        });

    };

    /**
     * Helper method to properly parse the forms
     * @param requestData
     * @returns FormData|string
     */
    var transformRequestDataToJSON = function(requestData) {
        // Jquery? Try to get the form
        if (requestData instanceof $) {
            requestData = requestData.get(0);
        }
        // Form? Try to get the form data by parsing it with jquery and giving it a little spin
        if (requestData instanceof HTMLElement && requestData.tagName === "FORM") {
            var $form = $(requestData);
            var formData = $form.serializeArray();
            requestData = new FormData();
            for(var i=0;i<formData.length;i++) {
                var key = formData[i].name;
                var value = formData[i].value;
                if (requestData.hasOwnProperty(key)) {
                    // Convert to array if it's not one
                    if (!(requestData[key] instanceof Array)) {
                        var prevValue = requestData[key];
                        requestData[key] = [prevValue];
                    }
                    requestData[key].push(value);
                } else {
                    requestData[key] = formData[i].value;
                }
            }
        }
        // Final transformations
        if (!requestData instanceof FormData) {
            requestData = JSON.stringify(requestData);
        }
        return requestData;
    };

    /*********************
     * EVENTS MANAGEMENT *
     *********************/

    /**
     * Method to bind eventListeners to the DOM objects
     */
    var bindBehaviours = function() {

        /** LETS USE DELEGATE EVENTS,
         * SINCE WE ARE PLANNING TO SWAP DOM CONTENT ASYNCHRONOUSLY **/


        // Article Dialog Open
        if ($articlesContainer.length > 0) {
            $articlesContainer.on("click", config.createArticleButtonSelector, function() {
                var $button = $(this);
                if ($button.is(".disabled")) { return false; }
                $button.addClass("disabled");
                // Define Callback to be called asynchronously after
                var onCompleteCallback = function() { $button.hide().removeClass("disabled"); };
                var $dialog = $articlesContainer.find(config.createArticleDialogSelector);
                openDialog($dialog, onCompleteCallback);
            });
        }

        // Product Dialog Open
        // @todo: lets not merge for now, in case we need to split behaviours later. I don't like 90% similar code chunks
        if ($productsContainer.length > 0) {
            $productsContainer.on("click", config.createProductButtonSelector, function() {
                var $button = $(this);
                if ($button.is(".disabled")) { return false; }
                $button.addClass("disabled");
                // Define Callback to be called asynchronously after
                var onCompleteCallback = function() { $button.hide().removeClass("disabled"); };
                var $dialog = $productsContainer.find(config.createProductDialogSelector);
                openDialog($dialog, onCompleteCallback);
            });
        }

        //
        $main.on( "submit", config.createDialogFormSelector, function(ev) {
            ev.preventDefault(); // Prevent form from following regular action

            var onSuccess = function() {},
                $form = $(this);

            if ($form instanceof $) {$($form);}

            var requestData = transformRequestDataToJSON($form);

            requestData["action"] = $form.data("action");
            requestData["target"] = $form.data("target");

            // If creating an article, we schedule a product list refresh on completion,
            // since new item should appear as an option on product creation
            if (requestData["target"] === "article") {
                onSuccess = refreshProductsList;
            }

            var targetHtmlContainer = $form.data("target-html-container");

            // Trigger async call manager
            performAsyncCall(requestData,targetHtmlContainer,onSuccess);
        });


        // Close dialog event helper
        $main.on( "click", config.closeDialogSelector, function(ev) {
            ev.preventDefault(); // Prevent in case is an <a>
            ev.stopPropagation(); // Prevent bubbling as well, just in case
            var $triggerElement = $(this),
                $targetToClose = $($triggerElement.data("target-close")),
                $targetToShow = $($triggerElement.data("target-show"));
            self.closeDialog($targetToClose, $targetToShow);
        });

        // Sell a product
        $productsContainer.on("click", config.sellProductButtonSelector, function(ev) {
            ev.preventDefault(); // Prevent in case is an <a>
            ev.stopPropagation(); // Prevent bubbling as well, just in case
            var $button = $(this),
                productName = $button.data("product-name");
            if ($button.is(".disabled")) { return false; }
                $button.addClass("disabled");
                sellProduct(productName);
        });

        // Add Stock to Article
        $articlesContainer.on("click", config.addStockToArticleButtonSelector, function(ev) {
            ev.preventDefault(); // Prevent in case is an <a>
            ev.stopPropagation(); // Prevent bubbling as well, just in case
            var $button = $(this),
                articleId = $button.data("article-id");
            if ($button.is(".disabled")) { return false; }
            $button.addClass("disabled");
            addStockToArticle(articleId);
        });

        // Loads the form of a ProductArticle into the product creation dialog
        $productsContainer.on("change", config.productArticlesSelectSelector, function (ev) {
            var $select = $(this),
                artId = $select.val();
            if (artId !== undefined && artId !== "") {
                var $option = $select.find("option[value='"+artId+"'");
                if ($option.length > 0) {
                    var articleName = $option.text();
                    loadProductArticleIntoDialog(articleName, artId, function() { $option.hide(); $select.val("").removeAttr("required"); } );
                }
            }

        });

        // Removes a previously injected ProductArticle creation form
        $productsContainer.on("click", config.removeProductArticleButtonSelector, function(ev) {
            ev.preventDefault(); // Prevent in case is an <a>
            ev.stopPropagation(); // Prevent bubbling as well, just in case
            var $button = $(this),
                targetSelector = $button.data("target");
            var $target = $(targetSelector);
            if ($target.length > 0 ) {
                // Remove the element
                $target.remove();
                // Get option to restore
                var $select = $productsContainer.find(config.productArticlesSelectSelector),
                    optionIdToRestore = $button.data("option-target-id");
                if ($select.length > 0 && optionIdToRestore !== undefined) {
                    $select.find("option[value='"+optionIdToRestore+"'").show();
                }
            }
        });

        // Reset source files to original state, we don't need a delegate binding on this one
        if ($resetButton.length > 0) {
            $resetButton.on("click",function() {
                var $button = $(this);
                if ($button.is(".disabled")) { return false; }
                $button.addClass("disabled");
                // Define Callback to be called asynchronously after
                var onCompleteCallback = function() { $resetButton.removeClass("disabled"); };
                resetSourceFiles(onCompleteCallback);
            });
        }

    };

    // GO! Let's make it start directly on instantiation
    init();

}