<?php

try {

    // Let's start
    error_reporting(E_ALL);
    require_once("../app/model/Warehouse.php");

} catch (\Exception $e ) {
    print_r($e->getMessage());
    print_r($e->getTraceAsString());
};

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Warehouse</title>
        <link rel="stylesheet" href="assets/css/app.css">
    </head>
    <body class="bg-light">

        <div id="notification" style="display:none;"></div>

        <header>
            <div class="navbar navbar-dark bg-dark shadow-sm">
                <h1 class="text-center text-white">
                    <i class="fas fa-industry"></i> Warehouse
                </h1>
                <button class="js-reset-source-files btn btn-warning btn-lg my-2 my-sm-0">Reset to Original</button>
            </div>
        </header>

        <main id="mainContainer" role="main">
            <div class="bg-light">
                <div class="container-fluid pt-3">
                    <div class="row">
                        <div id="articlesContainer" class="col-md-3 mb-3">
                            <?php include("../app/view/ArticlesList.inc");?>
                        </div>

                        <div class="col-md-9 mb-3">
                            <div class="row mt-0">
                                <div class="col-12">
                                    <div class="card mb-3 shadow-sm rounded-0">
                                        <div class="card-body">
                                            <h3 class="m-0">Products</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="productsContainer" class="row">
                                <?php include("../app/view/ProductsList.inc");?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>

        <div id="loadingOverlay">
            <div class="spinner-grow text-warning" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <!-- Additional libraries for minimal styling and UX purposes -->
        <script src="assets/js/jquery-3.5.1.min.js"></script>
        <link rel="stylesheet" href="assets/fontawesome/css/all.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <script src="assets/js/warehouse-app.js"></script>
    </body>
</html>
<script>
    /**
     * Synchronous instantiation. We need the libraries to be ready when this is called.
     * If I would be able to dedicate more time on this, a buffer should be implemented,
     * which would be read and flushed on dependencies load
     **/
    var WarehouseApp = new WarehouseApp({
        rootUrl : "<?php echo "http://".$_SERVER['SERVER_NAME']."/warehouse/";?>"
    });
</script>
