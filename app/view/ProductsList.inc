<?php

/*********************************************************
 * BLOCK THAT RENDERS THE PRODUCT LIST + CREATION DIALOG *
 *********************************************************/

$warehouseManagerInstance = \Warehouse\WarehouseManager::getInstance();

/********************************************
 * FETCH PRODUCTS, ITERATE AND COMPOSE HTML *
 ********************************************/

$products = $warehouseManagerInstance->getProducts();
/** @var \Warehouse\ProductItem $productItem */
foreach ($products as $productName => $productItem):?>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="card mb-3 shadow-sm rounded-0">
            <div class="card-header">
                <h4><i class="fas fa-chair"></i> <?php echo $productItem->name;?></h4>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Stock Needed:</li>
                <?php

                /********************************************
                 * FETCH PRODUCT ARTICLES, ITERATE AND COMPOSE HTML *
                 ********************************************/

                $productArticles = $productItem->productArticles();

                /** @var \Warehouse\ProductArticle $productArticleItem */
                foreach ($productArticles as $productArticleItem):?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <?php echo "#".$productArticleItem->art_id." - ".ucwords($productArticleItem->name);?>

                        <?php $enoughStock = $productArticleItem->enoughStock();?>
                        <span>
                            <?php if (!$enoughStock):?>
                                <small class="text-muted mr-2">Missing stock: <?php echo $productArticleItem->amountOf - $productArticleItem->stock;?></small>
                            <?php endif;?>
                            <span class="badge badge-pill <?php echo $enoughStock ? "badge-success" : "badge-danger" ;?>"><?php echo $productArticleItem->amountOf;?></span>
                        </span>
                    </li>
                <?php endforeach; ?>

            </ul>
            <?php $totalStock = $productItem->calculateStock();?>
            <div class="card-footer">
                <span class="mr-2 align-middle"><strong>Total Stock: <?php echo $totalStock;?></strong></span>
                <button type="button" data-product-name="<?php echo $productItem->name;?>" class="js-sell-product btn btn-primary float-right <?php if ($totalStock==0):?>disabled<?php endif;?>" <?php if ($totalStock==0):?>disabled<?php endif;?>><i class="fas fa-shopping-cart"></i> Sell</button>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<div class="js-create-product-dialog col-12 col-md-6 col-lg-4" style="display:none;">
    <div class="card mb-3 shadow-sm rounded-0">
        <form class="js-create-form" action="#" data-target="product" data-action="create" data-target-html-container="#productsContainer">
            <div class="card-header">
                <h4 class="d-inline-block">Create New Product</h4>
                <button type="button" class="close js-close-dialog" data-target-close=".js-create-product-dialog" data-target-show=".js-create-product" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input name="name" type="text" class="form-control" aria-describedby="nameHelp" minlength="3" maxlength="30" required />
                    <small id="nameHelp" class="form-text text-muted">From 3 to 30 chars.</small>
                </div>

                <div class="form-group">
                    <label for="articles">Articles Contained</label>
                </div>

                <div class="js-product-article-placeholder" style="display:none;"></div>

                <div class="form-group">
                    <select class="form-control js-product-articles-select" required>
                        <option value="">Choose an Article to add</option>
                        <?php $articleItems = $warehouseManagerInstance->getArticles();
                        foreach ($articleItems as $articleItem):?>
                            <option value="<?php echo $articleItem->art_id;?>"><?php echo ucwords($articleItem->name);?></option>
                        <?php endforeach;?>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary btn-block">Create</button>
            </div>
        </form>

        <!-- ProductArticle Form template -->

        <template class="js-product-article-template" style="display: none;">
            <div id="productArticle-%artId%" class="js-product-article-item form-row mb-2">
                <div class="form-group col-7 d-flex align-items-end">
                    <label class="ml-1" for="productArticle-%artId%">#%artId% - %productArticleName%</label>
                    <input class="js-art-id" type="hidden" name="productArticle-%artId%" value="%artId%" required>
                </div>
                <div class="form-group col-3">
                    <label for="productArticleStock-%artId%"><small>Amount needed</small></label>
                    <input class="js-amount-of" type="number" class="form-control" name="productArticleStock-%artId%" min="1" max="999" required>
                </div>
                <div class="form-group col-2 d-flex align-items-end">
                    <button class="js-remove-product-article btn btn-outline-danger" data-option-target-id="%artId%" data-target="#productArticle-%artId%"><i class="fas fa-trash"></i></button>
                </div>
            </div>
        </template>
    </div>
</div>

<div class="col-12">
    <button class="js-create-product btn btn-lg btn-block btn-success"><i class="fas fa-plus"></i> Create Product</button>
</div>
