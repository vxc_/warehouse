# The Warehouse 
Implement a Warehouse software

## Following assigment values
As requested, simplicity, readability, maintainability and testability are the main values to take in consideration while solving the assignment.
I've tried to pay special attention to them, even though they are values I think I have already internalised over the years and I think they can be found in all the code I write: 
Robert C. Martin's "Clean Code" has always been a huge reference for my code, :P.

On the other hand, general performance, micro-optimisation and algorithm efficiency (Big O Notation) has not been a priority for the task.

Let's iterate through these values so I can comment a bit how they have been taken in consideration:

### Simplicity
Falling into over-engineering is quite easy, it's a thin line that is easy to cross (lol). On the other hand, following too much the KISS patterns sometimes leads to non-versatile, hard to maintain code due to the seek of extreme simplicity.
I've tried to stay in the middle coding a simple objects architecture.

### Readability
Readability is basic for me in the code. I hate reading code I can not understand, so I take very seriously the organisation of the code, 
it's documentation (yes, even if it forces you to change it if you change the code), and above all proper naming for pretty much everything.
Supporting this theory, I've tried to code very easy-to-read javascript code, since as you would know, there are infinite ways to write JS, not all of them user-friendly xd

### Maintainability
Above all, a well thought and structured architecture is easy to maintain. 
Once you have that, I've tried to avoid monolithic code, repeated code, wreck-trains and very big functions

### Testability
I like to write rock-solid code by performing integrity/type checks in the code flow and proper error handling

## Languages chosen

The task has been developed as a Web Application, having as strengths:
* No installation needed
* Device agnostic
* "Portable"

The Web Application has an asynchronous approach to improve the UX.

The languages chosen are PHP for the backend/objects structure and html/css/JS for the client-side, interface and UX.

* **Why PHP?** It's a non-strict, non-compiled object oriented language, so it's easy to write decent code FAST, which is what I needed: My free time nowadays is very rare to find 
* **Why Javascript?** I didn't want to choose any JS Framework in particular (but jQuery, just to speed up things a bit). 
I preferred to write a sort of APP structure scaffolding in pure JS to show you how I like to structure the code and access to the DOM 

## Functionalities implemented (Required)
* Articles list and the available stock
* Products list and the available stock depending on the articles they are composed from
* Sell a product action if elegible 

### Additional Functionalities
* Product creation 
* Article creation
* Article stock increase
* Reset source files
* Helpers: Notification system

## Final Thoughts
I've had a lot of fun while implementing the software, it always feels good to build something from scratch! 
If there is weird stuff inside, please, don't take it too much in consideration 🙏. I've coded this in a total of 7 hours during 3 different nights on work days, I was really tired xd
