<?php
namespace Warehouse {

    /**
     * Class WarehouseManager
     * A Singleton Factory to manage Article and Product Items
     */
    class WarehouseManager {

        const ARTICLES_SRC_PATH = "/../../src/inventory.json";
        const PRODUCTS_SRC_PATH = "/../../src/products.json";

        // Stored original version of the files, in order to perform the reset if requested.
        const ORIG_ARTICLES_SRC_PATH = "/../../src/orig_inventory.json";
        const ORIG_PRODUCTS_SRC_PATH = "/../../src/orig_products.json";



    /**********************
     * CONTROL PROPERTIES *
     **********************/

        protected $_stockLoading = false;
        protected $_stockLoaded = false;

        /**
         * Articles Container
         * @var $articlesStock ArticleItem[]
         */
        protected $articles = array();

        /**
         * Products Container
         * @var $productsStock ProductItem[]
         */
        protected $products = array();



    /*************************
     * INSTANTIATION METHODS *
     *************************/

        /**
         * Self Manager singleton
         * @var \Warehouse\WarehouseManager
         */
        protected static $_instance = null;


        /**
         * Singleton access
         * @return \Warehouse\WarehouseManager
         */
        public static function getInstance() {
            if (!static::$_instance) static::$_instance = new static();
            return static::$_instance;
        }

        /**
         * Protected constructor
         */
        protected function __construct() {}

        /***********
         * LOADERS *
         ***********/


        /**
         * Loads the Stock And store it in the Manager
         * @param bool $forceLoad
         * @return bool
         */
        public function loadStock($forceLoad = false) {
            if ($this->_stockLoading || ( $this->_stockLoaded && !$forceLoad ) ) { return false; }
            $this->_stockLoading = true;
            try {
                if ($this->loadArticles() && $this->loadProducts()) {
                    $this->_stockLoaded = true;
                }
            } catch(\Exception $e) {
                print_r($e);
                exit;
            }
            $this->_stockLoading = false;
        }

        /**
         * Loads the Articles into the Manager
         * @return ArticleItem[]
         * @throws \Exception
         */
        protected function loadArticles() {
            $filePath = dirname(__FILE__).self::ARTICLES_SRC_PATH;
            $fileContentAsString = $this->readFile($filePath);
            $fileContentJson = json_decode($fileContentAsString, true);
            if (is_array($fileContentJson["inventory"]) && isset($fileContentJson["inventory"])) {
                foreach ($fileContentJson["inventory"] as $jsonDataItem) {
                    $this->articles[$jsonDataItem["art_id"]] = new ArticleItem($jsonDataItem);
                }
                return $this->articles;
            } else {
                throw new \Exception("Content of the file doesnt seems to be a properly formed JSON object");
            }
        }

        /**
         * Loads the Products into the Manager
         * @return ProductItem[]
         * @throws \Exception
         */
        protected function loadProducts() {
            $filePath = dirname(__FILE__).self::PRODUCTS_SRC_PATH;
            $fileContentAsString = $this->readFile($filePath);
            $fileContentJson = json_decode($fileContentAsString, true);
            if (is_array($fileContentJson["products"]) && isset($fileContentJson["products"])) {
                foreach ($fileContentJson["products"] as $jsonDataItem) {
                    $this->products[$jsonDataItem["name"]] = new ProductItem($jsonDataItem);
                }
                return $this->products;
            } else {
                throw new \Exception("Content of the file doesnt seems to be a properly formed JSON object");
            }
        }

        /***********
         * GETTERS *
         ***********/

        /** Articles Getter, for convenience **/
        public function getArticles() {
            if (!$this->_stockLoaded) { $this->loadStock(); }
            return $this->articles;
        }

        /** Empty Article Constructor **/
        public function emptyArticleItem() {
            return new ArticleItem(array());
        }

        /** Producs Getter, for convenience **/
        public function getProducts() {
            if (!$this->_stockLoaded) { $this->loadStock(); }
            return $this->products;
        }

        /** Empty Article Constructor **/
        public function emptyProductItem() {
            return new ProductItem(array());
        }

        /**
         * @param int $art_id
         * @return ArticleItem|null
         */
        public function getArticleById($art_id) {
            if (!$this->_stockLoaded) { $this->loadStock(); }
            return isset($this->articles[$art_id]) ? $this->articles[$art_id] : null;
        }

        /**
         * @param string $productName
         * @return ProductItem|null
         */
        public function getProductByName($productName) {
            if (!$this->_stockLoaded) { $this->loadStock(); }
            return isset($this->products[$productName]) ? $this->products[$productName] : null;
        }

        /***********
         * SETTERS *
         **********

        /*
         * @param ArticleItem $articleItem
         */
        public function setArticleItem(ArticleItem $articleItem) {
            if (!$this->_stockLoaded) { $this->loadStock(); }
            $this->articles[$articleItem->art_id] = $articleItem;
        }

        /**
         * @param ProductItem $productItem
         */
        public function setProductItem(ProductItem $productItem) {
            if (!$this->_stockLoaded) { $this->loadStock(); }
            $this->products[$productItem->name] = $productItem;
        }

        /*****************************
         * FILE MANIPULATION METHODS *
         *****************************/

        /**
         * Ideally, this can be migrated to a Driver structure, implementing an interface with read/write methods.
         * $this->storageDriver()->read();
         * $this->storageDriver()->write();
         * This would eventually ease a possible migration from a file-source system to another storage system.
         *
         * I don't know if I'm going to have enough time though, so let's let it be for now.
         */

        /**
         * Helper to read a file
         * @param $filePath
         * @return false|string
         * @throws \Exception
         */
        public function readFile($filePath) {
            if (file_exists($filePath) && is_readable($filePath)) {
                $fileContentAsString = file_get_contents($filePath);
                return $fileContentAsString;
            } else {
                throw new \Exception("File ".$filePath. " Not found or not enough permissions to read it");
            }
        }

        /**
         * Helper to write a file
         * @param $filePath
         * @param $fileContent
         * @return bool
         * @throws \Exception
         */
        public function writeFile($filePath, $fileContent) {
            // Check file accesibility and write
            if (is_writable($filePath)) {
                file_put_contents($filePath, $fileContent);
                return true;
            } else {
                throw new \Exception("File ".$filePath. " is not writable");
            }
        }

        /**
         * Composes the articles Array and persists it into inventory.json
         * @return bool
         * @throws \Exception
         */
        public function flushAndWriteArticles() {
            $filePath = dirname(__FILE__).self::ARTICLES_SRC_PATH;
            $fileOutputArr = array("inventory" => array());
            //Iterate and compose
            /** @var \Warehouse\ArticleItem $articleItem */
            foreach ($this->getArticles() as $articleItem) {
                $fileOutputArr["inventory"][] = $articleItem->toArray();
            }
            return $this->writeFile($filePath, json_encode($fileOutputArr));
        }

        /**
         * Composes the products Array and persists it into products.json
         * @return bool
         * @throws \Exception
         */
        public function flushAndWriteProducts() {
            $filePath = dirname(__FILE__).self::PRODUCTS_SRC_PATH;
            $fileOutputArr = array("products" => array());

            //Iterate and compose
            /** @var \Warehouse\ProductItem $productItem */
            foreach ($this->getProducts() as $productItem) {
                $fileOutputArr["products"][] = $productItem->toArray();
            }

            return $this->writeFile($filePath, json_encode($fileOutputArr));

        }

        /**
         * Resets the inventory.json file with the orig_products.json content
         * @throws \Exception
         */
        public function resetArticlesSource() {
            $origFilePath = dirname(__FILE__).self::ORIG_ARTICLES_SRC_PATH;
            $outputFilePath = dirname(__FILE__).self::ARTICLES_SRC_PATH;
            return $this->resetSource($origFilePath,$outputFilePath);
        }

        /**
         * Resets the products.json file with the orig_products.json content
         * @throws \Exception
         */
        public function resetProductsSource() {
            $origFilePath = dirname(__FILE__).self::ORIG_PRODUCTS_SRC_PATH;
            $outputFilePath = dirname(__FILE__).self::PRODUCTS_SRC_PATH;
            return $this->resetSource($origFilePath,$outputFilePath);
        }

        /**
         * Helper to write a file with the content of another one
         * @param $origPath
         * @param $outputPath
         * @throws \Exception
         */
        protected function resetSource($origPath, $outputPath) {
            $fileContentAsString = $this->readFile($origPath);
            $this->writeFile($outputPath,$fileContentAsString);
        }

    }


/*************************************
 * OBJECTS CONTROLLED BY THE MANAGER *
 *************************************/


    /**
     * Class ArticleItem
     * @package Warehouse
     */
    class ArticleItem {

    /****************************
     * BASE - PUBLIC PROPERTIES *
     ****************************/

        /**
         * @var $art_id int
         */
        public $art_id;

        /**
         * @var $name string
         */
        public $name;

        /**
         * @var $stock int
         */
        public $stock;


    /**********************
     * CONTROL PROPERTIES *
     **********************/

        /**
         * Array to validate mandatory properties of the object
         * @var array $mandatoryProperties
         */
        protected $mandatoryProperties = ["art_id","name","stock"];
        protected $_loaded = false;


    /******************
     * INITIALISATION *
     ******************/

        /**
         * ArticleItem constructor.
         * @param array $data
         * @throws \Exception
         */
        public function __construct(array $data) {
            $this->validateAndLoadData($data);
        }

        /**
         * Object properties loader and validator
         * @param array $data
         * @return bool
         * @throws \Exception
         */
        protected function validateAndLoadData(array $data) {
            if (empty($data)) { return false; }
            // Validate the presence of the properties in the provided array
            $this->validateData($data);
            foreach ($data as $propertyName => $value) {
                // Load the property
                $this->{$propertyName} = $value;
            }
            $this->_loaded = true;
        }

        /**
         * Object properties validator
         * @param array $data
         * @return bool
         * @throws \Exception
         */
        protected function validateData(array $data) {
            // Validate the presence of the properties in the provided array
            foreach ($this->mandatoryProperties as $propertyName) {
                if (!in_array($propertyName, array_keys($data), true)) {
                    throw new \Exception("Property ".$propertyName. " missing in provided data array ".json_encode($data));
                }
            }
            return true;
        }

    /***********
     * HELPERS *
     ***********/

        /**
         * Returns the object as an array o properties
         * @return array
         */
        public function toArray() {
            return array(
                "art_id" => $this->art_id,
                "name" => $this->name,
                "stock" => $this->stock,
            );
        }

    }


    /**
     * Class ProductItem
     * @package Warehouse
     */
    class ProductItem implements iSellable {

    /****************************
     * BASE - PUBLIC PROPERTIES *
     ****************************/

        /**
         * @var $name string
         */
        public $name;

        /**
         * json parsed array of the contain_articles object from the file
         * @var $containing_articles array
         */
        public $contain_articles = array();


    /**********************
     * CONTROL PROPERTIES *
     **********************/

        protected $mandatoryProperties = ["name","contain_articles"];
        protected $_productArticlesLoaded = false;


        /********************
         * ATTACHED OBJECTS *
         ********************/

        /**
         * Array of ProductArticle items, composed from $contain_articles
         * @var $containedProductArticles ProductArticle[]
         */
        protected $containedProductArticles = array();


        /******************
         * INITIALISATION *
         ******************/

        /**
         * On-Demand bridge Constructor
         * @return ProductArticle[]
         * @throws \Exception
         */
        public function productArticles() {
            if (!$this->_productArticlesLoaded) { $this->loadProductArticles(); }
            return $this->containedProductArticles;
        }

        /**
         * ProductArticle loader
         * @important Care with this method if called during load process, it can trigger load again and generate circular references
         * @return bool
         * @throws \Exception
         */
        protected function loadProductArticles() {
            if ($this->_productArticlesLoaded) { return false; }
            /** @var $containing_article array **/
            foreach ($this->contain_articles as $contained_article) {
                if (!is_array($contained_article) || !isset($contained_article["art_id"])) {
                    throw new \Exception("Invalid containing_article format: art_id not provided in ".json_encode($containing_article));
                }
                $warehouseManagerInstance = \Warehouse\WarehouseManager::getInstance();
                $articleItem = $warehouseManagerInstance->getArticleById($contained_article["art_id"]); // Care, can trigger load again
                if ($articleItem instanceof ArticleItem) {
                    $articleItemInfo = $articleItem->toArray();
                    $productArticle = new ProductArticle($articleItemInfo, $this);
                    $productArticle->amountOf = $contained_article["amount_of"];
                    $this->containedProductArticles[$productArticle->art_id] = $productArticle;
                } else {
                    throw new \Exception("Unable to load ArticleItem with provided id: ".$contained_article["art_id"]);
                }
            }
            $this->_productArticlesLoaded = true;
        }

        /**
         * ProductItem constructor.
         * @param array $data
         * @throws \Exception
         */
        public function __construct(array $data) {
            $this->validateAndLoadData($data);

            // Load on demand?
            // $this->loadProductArticles();
        }


        /**
         * Object properties loader and validator
         * @param array $data
         * @return bool
         * @throws \Exception
         */
        protected function validateAndLoadData(array $data) {
            if (empty($data)) { return false; }
            // Validate the presence of the properties in the provided array
            $this->validateData($data);
            foreach ($data as $propertyName => $value) {
                // Load the property
                $this->{$propertyName} = $value;
            }
            $this->_loaded = true;
        }

        /**
         * Object properties validator
         * @param array $data
         * @return bool
         * @throws \Exception
         */
        protected function validateData(array $data) {
            // Validate the presence of the properties in the provided array
            foreach ($this->mandatoryProperties as $propertyName) {
                if (!in_array($propertyName, array_keys($data), true)) {
                    throw new \Exception("Property ".$propertyName. " missing in provided data array ".json_encode($data));
                }
            }
            return true;
        }


        /*************************
         * STOCK-RELATED METHODS *
         *************************/

        /**
         * Calculates the actual stock of each ProductArticle in the Product
         * @return int
         * @throws \Exception
         */
        public function calculateStock() {
            $totalStock = 0;
            $idx = 0; // Pivot to write on the first lap
            foreach( $this->productArticles() as $productArticle) {
                $productArticleStock = $productArticle->calculateTotalStock();
                /** Lets get the lowest available stock value to determine the total stock **/
                if ($idx == 0 || $productArticleStock <= $totalStock) {
                    $totalStock = $productArticleStock;
                }
                $idx++;
            }
            return $totalStock;
        }


        /**
         * Getter of the elegibility of the product to be sold depending on the present stock
         * @return bool
         * @throws \Exception
         */
        public function canBeSold() {
            return $this->calculateStock() > 0;
        }

        /**
         * Method that performs the sale of a product
         *
         * THIS IS THE MOST IMPORTANT/SENSITIVE METHOD OF THE WHOLE TASK
         * ## Thoughts and concerns (ideally):
         * - Avoid Race conditions
         * - Atomicity of the operation should be seek / required
         * ## Possible Solutions:
         * - Implement a semaphore / locking system (distributed if more than 1 machine is involved)
         *
         * @return bool
         * @throws \Exception
         */
        public function sell() {

                // LOCK WRITE OPERATIONS - LOCK SEMAPHORE, ecc

                // Renew the stock, just in case something happened in the meantime
                $warehouseManager = \Warehouse\WarehouseManager::getInstance();
                $warehouseManager->loadStock(true);


                // Check stock
                if ($this->canBeSold()) {

                    foreach( $this->productArticles() as $productArticle ) {

                        // Decrease stock on Articles of the Product
                        $articleItem = $warehouseManager->getArticleById($productArticle->art_id);
                        $articleItem->stock -= $productArticle->amountOf;

                    }

                    // Persist the sale on the articles stock
                    $warehouseManager->flushAndWriteArticles();

                } else {
                    throw new \Exception("Product not elegible to be sold");
                }

                // UNLOCK WRITING OPERATIONS AGAIN

            return true;
        }

        /***********
         * HELPERS *
         ***********/

        /**
         * Returns the object as an array o properties
         * @return array
         */
        public function toArray() {
            return array(
                "name" => $this->name,
                "contain_articles" => (array) $this->contain_articles,
            );
        }

    }

    /***************************
     * PRODUCT-ATTACHED OBJECT *
     ***************************/

    /**
     * Class ProductArticle
     *
     * @extends ArticleItem
     * @package Warehouse
     */
    class ProductArticle extends ArticleItem {

        public $amountOf;

        /**
         * Parent reference
         * @var \Warehouse\ProductItem $_productItem
         */
        protected $_productItem = null;

        /**
         * ArticleItem constructor.
         * @param array $data
         * @param \Warehouse\ProductItem $productItem
         * @throws \Exception
         */
        public function __construct(array $data, \Warehouse\ProductItem $productItem) {
            $this->_productItem = $productItem;
            // Call parents constructor
            parent::__construct($data);
        }

        /**
         * Checks if there is enough stock of the article
         * @return bool
         */
        public function enoughStock() {
            return $this->stock >= $this->amountOf;
        }

        /**
         * Calculates the total stock of the productArticle
         * @important Used to determine the total stock of the product.
         * @return float|int
         */
        public function calculateTotalStock() {
            if ($this->amountOf == 0) { return 0; }
            return floor($this->stock/$this->amountOf);
        }

        /**
         * Returns the object as an array o properties
         * @return array
         */
        public function toArray() {
            return array(
                "art_id" => $this->art_id,
                "amount_of" => $this->amountOf
            );
        }

    }


    /**************
     * INTERFACES *
     **************/

    /**
     * Interface iSellable
     * @package Warehouse
     */
    interface iSellable {
        public function sell();
    }

}