<?php

/*********************************************************
 * BLOCK THAT RENDERS THE ARTICLES LIST + CREATION DIALOG *
 *********************************************************/

$warehouseManagerInstance = \Warehouse\WarehouseManager::getInstance();

;?>
<div id="articles" class="card rounded-0 mb-3">
    <div class="card-body">
        <h3>Articles</h3>
        <ul class="list-group list-group-flush">
            <?php

            /********************************************
             * FETCH ARTICLES, ITERATE AND COMPOSE HTML *
             ********************************************/

            $articles = $warehouseManagerInstance->getArticles();

            /** @var \Warehouse\ArticleItem $articleItem */
            foreach ($articles as $artId => $articleItem):?>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <?php echo "#".$articleItem->art_id." - ".ucwords($articleItem->name);?>
                    <div class="d-inline-block">
                        Stock:
                        <span class="mr-1 badge badge-pill <?php echo empty($articleItem->stock) ? "badge-danger" : "badge-primary";?>"><?php echo $articleItem->stock;?></span>
                        <button type="button" class="js-add-stock-to-article btn btn-outline-success btn-sm" data-article-id="<?php echo $articleItem->art_id;?>"><i class="fas fa-plus"></i></button>
                    </div>
                </li>
            <?php endforeach; ?>

        </ul>
    </div>
</div>

<div class="js-create-article-dialog card rounded-0 mb-3" style="display:none;">
    <form class="js-create-form" action="#" data-target="article" data-action="create" data-target-html-container="#articlesContainer">
        <div class="card-header">
            <h4 class="d-inline-block">Create New Article</h4>
            <button type="button" class="close js-close-dialog" data-target-close=".js-create-article-dialog" data-target-show=".js-create-article" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label for="name">Name</label>
                <input name="name" type="text" class="form-control" aria-describedby="nameHelp" minlength="3" maxlength="30" required />
                <small id="nameHelp" class="form-text text-muted">From 3 to 30 chars.</small>
            </div>
            <div class="form-group">
                <label for="stock">Stock</label>
                <input name="stock" type="number" class="form-control" id="stock" min="0" max="999" required />
            </div>
            <button type="submit" class="btn btn-primary btn-block">Create</button>
        </div>
    </form>
</div>
<div class="clearfix">
    <button class="js-create-article btn btn-lg btn-block btn-success"><i class="fas fa-plus"></i> Create Article</button>
</div>
