<?php
/*******************************************************************
* Abstract Controller to centralize minimum xhr request operations *
********************************************************************/

/**
 * Class ActionDispatcher
 */
abstract class ActionDispatcher {

    const RESPONSE_TYPE = "application/json";
    const RESPONSE_CHARSET = "utf-8";
    const REQUEST_ACCEPTED_METHOD = "POST";

    /**
     * The response object
     * @var array $response *
     */
    protected $response;

    /**
     * Array
     * @var mixed
     */
    protected $postParams = array();

    /**
     * ActionDispatcher constructor.
     *
     * - Checks if request method is valid
     * - Populates the postParams property
     * - Sets response headers
     * - Prevents from possible XSS injection by parsing post data
     * - Encodes the response object
     *
     * @throws Exception
     */
    public function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] !== self::REQUEST_ACCEPTED_METHOD) {
            throw new \Exception("Just ".self::REQUEST_ACCEPTED_METHOD." requests are accepted" );
        }

        header("Content-type: ".self::RESPONSE_TYPE.";charset=" .self::RESPONSE_CHARSET );

        // Set response as success by default
        $this->response = array(
            "success" => true,
        );

        //Save POST params

        $postStr = file_get_contents('php://input');
        $postParamsArr = json_decode($postStr, TRUE);

        $this->postParams = $postParamsArr;

        // Minimal security layer
        // Prevent XSS on POST params
        $this->preventPostXSS();

        $this->main();

        echo json_encode($this->response);

    }


    /**
     * MAIN ABSTRACT FUNCTION
     */
    abstract public function main();


    /********************
     * SECURITY HELPERS *
     ********************/


    /**
     * Filters post parameters to prevent XSS, must be called specifically
     * INFO: Only first level of arrays gets filtered
     */
    protected function preventPostXSS() {
        foreach ($this->postParams as $k=>$v) {
            if (!$this->isParameterKeyValid($k)) {
                unset($this->postParams[$k]);
            }
            if (is_string($this->postParams[$k])) {
                $this->postParams[$k] = $this->filterXSS($v);
            } elseif(is_array($this->postParams)) {
                foreach ($this->postParams[$k] as $k2=>$v2) {
                    if (is_string($this->postParams[$k][$k2])) {
                        $this->postParams[$k][$k2] = $this->filterXSS($v2);
                    }
                }
            }
        }

    }

    /**
     * Helper Check if a parameter key is invalid.
     * Filters ', ", \, < and >
     * @param $key
     * @return bool
     */
    protected function isParameterKeyValid($key) {
        if (preg_match("/[\\'\\\"\\<\\>]+/",$key)) {
            return false;
        }
        return true;
    }

    /**
     * Filters a string to prevent XSS:
     * - Removes script tag
     * - Removes "fromCharcode" to prevent things like alert(String.fromCharCode(21,23,83)
     * @param string $string
     * @return mixed
     */
    protected function filterXSS($string) {
        if (!is_string($string)) return $string;
        $string = preg_replace("/[\\\\><;]/","",strip_tags($string));
        $string = preg_replace("/([^a-zA-Z0-9\\_\\-\\.])script/i", "$1", $string);
        $string = preg_replace("/fromCharCode/i","",$string);
        return $string;
    }

}