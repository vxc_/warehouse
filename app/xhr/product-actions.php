<?php
require_once("../model/Warehouse.php");
require_once("controller/AbstractActionDispatcher.php");

class ProductActions extends ActionDispatcher  {

    public function main() {

        /**
         * CONCEPT CLARIFICATIONS:
         * Ideally this should be designed as a RESTFUL controller,
         * dispatching methods depending on the request method provided.
         */

        /**
         * Encapsulate actions dispatching to catch all possible errors on the flow and send them back to notify
         */
        try {
            // Dispatch by action name
            switch ($this->postParams["action"]) {
                case "create":
                    $this->createProduct();
                    break;
                case "sell":
                    $this->sellProduct();
                    break;
                case "get":
                    $this->getProductsListHtml();
                    break;
                default:
                    $this->response["success"] = false;
                    $this->response["error"] = "Action to dispatch not found";
                    break;
            }
        }catch (\Exception $e) {
            $this->response["success"] = false;
            $this->response["error"] = $e->getMessage();
        }

    }

    /**
     * Creates a product
     * @throws Exception
     */
    protected function createProduct() {

        if (!isset($this->postParams["name"])) {
            throw new \Exception("name param not provided on article creation");
        }

        $warehouseManagerInstance = \Warehouse\WarehouseManager::getInstance();

        $products = $warehouseManagerInstance->getProducts();
        $newProductName = $this->postParams["name"];

        // Check if name is already present
        if (in_array($newProductName, array_keys($products))) {
            throw new \Exception("Provided Name Param $newProductName already exist!");
        }

        // Build empty Product
        $emptyProductItem = $warehouseManagerInstance->emptyProductItem();
        $emptyProductItem->name = $this->postParams["name"];


        // Compose the ProductArticles items arrays
        $productArticlesIdKey = 'productArticle-';
        $productArticlesStockIdKey = 'productArticleStock-';

        // @todo Lol, what a wreck train. I will refactor this if I've got enough time
        $matches = array_filter(array_keys($this->postParams), function($var) use ($productArticlesIdKey) {
            return preg_match("/\b$productArticlesIdKey\b/i", $var); });

        // We just need the string values ["productArticle-1", "productArticle-3", "productArticle-7"]
        $matches = array_values($matches);

        $contain_articles = array();

        // Iterate over matches and compose contained_articles array
        /** @var $idToParse string "productArticle-{id}" */
        foreach ($matches as $idToParse) {

            // Parse id
            $id = explode($productArticlesIdKey, $idToParse)[1];

            // Needs to be a number
            if (!is_numeric($id)) {
                throw new \Exception("Product Article Id parsed is not a number! ($id)");
            }

            // Check for stock param on post params
            if ( !isset($this->postParams[$productArticlesStockIdKey.$id]) ) {
                throw new \Exception("Stock Param for Product Article with Id $id not found!");
            }

            $amountOf = $this->postParams[$productArticlesStockIdKey.$id];

            // Lets create the ProductArticle array with the info
            $contained_article = array(
                "art_id" => $id,
                "amount_of" => $amountOf
            );

            // Append Contain Article array
            $contain_articles[]=$contained_article;

        }

        if (empty($contain_articles)) {
            throw new \Exception("Product should countain at least 1 Article!");
        }

        // Set contain articles array
        $emptyProductItem->contain_articles = $contain_articles;

        // Compose ProductArticle items.
        $emptyProductItem->productArticles();

        // Append New item to the collection
        $warehouseManagerInstance->setProductItem($emptyProductItem);

        // Flush and write the file
        $warehouseManagerInstance->flushAndWriteProducts();

        // Force Reload from file
        $warehouseManagerInstance->loadStock(true);

        // Send back refreshed HTML
        $this->getProductsListHtml();

        $this->response["notification"] = "Succesfully created product ".ucwords($emptyProductItem->name)."!";

    }

    /**
     * Sells a Product if elegible
     * @throws Exception
     */
    protected function sellProduct() {

        if (!$this->postParams["name"]) { throw new \Exception("No product name provided"); }
        $warehouseManagerInstance = \Warehouse\WarehouseManager::getInstance();

        // Load product
        $product = $warehouseManagerInstance->getProductByName($this->postParams["name"]);
        if (!$product instanceof \Warehouse\ProductItem) { throw new \Exception("Unable to load the product"); }

        // Perform sale
        $product->sell();

        // Force Reload from file
        $warehouseManagerInstance->loadStock(true);

        // Send back refreshed HTML
        $this->getProductsListHtml();

        $this->response["notification"] = "Succesfully sold ".ucwords($product->name)."!";

    }

    /**
     * Returns the Product list HTML as a response property
     */
    protected function getProductsListHtml() {

        ob_start();
        include("../view/ProductsList.inc");
        $includeContent = ob_get_clean();

        $this->response["html"] = $includeContent;
    }

}

try {
    $actionDispatcher = new ProductActions();
} catch (\Exception $e) { throw $e; }


?>