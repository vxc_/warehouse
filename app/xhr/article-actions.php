<?php
require_once("../model/Warehouse.php");
require_once("controller/AbstractActionDispatcher.php");

class ArticleActions extends ActionDispatcher  {

    public function main() {

        /**
         * CONCEPT CLARIFICATIONS:
         * Ideally this should be designed as a RESTFUL controller,
         * dispatching methods depending on the request method provided.
         */

        /**
         * @important Encapsulate action dispatching to catch all possible errors on the flow
         * and send them back to notify the client
         */
        try {
            // Dispatch by action name
            switch ($this->postParams["action"]) {
                case "create":
                    $this->createArticle();
                    break;
                case "add":
                    $this->addStockToArticle();
                    break;
                case "get":
                    $this->getArticlesListHtml();
                    break;
                case "reset":
                    $this->resetSourceFiles();
                    break;
                default:
                    $this->response["success"] = false;
                    $this->response["error"] = "Action to dispatch not found";
                    break;
            }
        }catch (\Exception $e) {
            $this->response["success"] = false;
            $this->response["error"] = $e->getMessage();
        }

    }

    /**
     * Creates an Article with the provided info
     * @throws Exception
     */
    protected function createArticle() {

        if (!isset($this->postParams["name"]) || !isset($this->postParams["stock"])) {
            throw new \Exception("name or stock params not provided on article creation");
        }

        $warehouseManagerInstance = \Warehouse\WarehouseManager::getInstance();

        $articles = $warehouseManagerInstance->getArticles();

        $emptyArticleItem = $warehouseManagerInstance->emptyArticleItem();
        $emptyArticleItem->art_id = count($articles) + 1;
        $emptyArticleItem->name = $this->postParams["name"];
        $emptyArticleItem->stock = $this->postParams["stock"];

        /**
         * @NOTE Writing behaviour might be migrated to a  $emptyArticleItem->save() method
         */

        // Add Article to the Manager
        $warehouseManagerInstance->setArticleItem($emptyArticleItem);

        // Flush and write the file
        $warehouseManagerInstance->flushAndWriteArticles();

        // Force Reload from file
        $warehouseManagerInstance->loadStock(true);

        $this->getArticlesListHtml();

        $this->response["notification"] = "Succesfully created article ".ucwords($emptyArticleItem->name)."!";

    }

    /**
     * Adds stock to the provided article
     * @throws Exception
     */
    protected function addStockToArticle() {
        if (!$this->postParams["art_id"]) { throw new \Exception("No Article Id provided"); }
        $warehouseManagerInstance = \Warehouse\WarehouseManager::getInstance();

        // Load product
        $articleItem = $warehouseManagerInstance->getArticleById($this->postParams["art_id"]);
        if (!$articleItem instanceof \Warehouse\ArticleItem) { throw new \Exception("Unable to load the article"); }

        $articleItem->stock += 1;

        // Flush and write the file
        $warehouseManagerInstance->flushAndWriteArticles();

        // Force Reload from file
        $warehouseManagerInstance->loadStock(true);

        $this->getArticlesListHtml();
    }

    /**
     * Resets the source files.
     * Yes, this could've been place somewhere else xd
     * @throws Exception
     */
    protected function resetSourceFiles() {
        $warehouseManagerInstance = \Warehouse\WarehouseManager::getInstance();
        $warehouseManagerInstance->resetArticlesSource();
        $warehouseManagerInstance->resetProductsSource();
        // Do not reset, we might have some dependency errors if we do so
        //$this->getArticlesListHtml();
    }

    /**
     * Returns the Article list HTML as a response property
     */
    protected function getArticlesListHtml() {

        ob_start();
        // Save content into a variable
        include("../view/ArticlesList.inc");
        $includeContent = ob_get_clean();

        $this->response["html"] = $includeContent;
    }

}

// Arg, I'd love to make a decent action dispatching system. No time though, focus.
try {
    $actionDispatcher = new ArticleActions();
} catch (\Exception $e) { throw $e; }

?>